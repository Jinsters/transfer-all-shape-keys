# MIT License

# Copyright (c) 2019 Jinsters

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


bl_info = {
	"name": "Transfer All Shape Keys",
	"author": "Jinsters",
	"version": (0, 1, 0),
	"blender": (2, 79, 0),
	"location": "Properties > Data > Shape Keys",
	"description": "Transfer all shapes from one selected mesh object to the active mesh object",
	"warning": "",
	"wiki_url": "",
	"category": "Object",
	}

import bpy
from bpy import (data, context, ops)


def main(self, context):
	active_object = context.active_object
	active_object_selected = False
	selected_object = None

	# Get selected objects
	for obj in context.selected_objects:
		if obj == active_object:
			active_object_selected = True
		else:
			selected_object = obj
	
	if active_object_selected is False:
		self.report({"ERROR"}, "Expected one other selected mesh object to copy from")
		return {"CANCELLED"}

	if selected_object.type != "MESH":
		self.report({"ERROR"}, "Other object is not a mesh")
		return {"CANCELLED"}

	if selected_object.data.shape_keys is None:
		self.report({"ERROR"}, "Other object has no shape keys")
		return {"CANCELLED"}
	
	# Check objects have same number of vertices
	active_verts = len(active_object.data.vertices)
	selected_verts =len(selected_object.data.vertices)

	if selected_verts != active_verts:
		self.report({"ERROR"}, "Objects have different number of vertices:\n" + \
			active_object.name + " (" + str(active_verts) + ")\n" + \
			selected_object.name + " (" + str(selected_verts) + ")")
		return {"CANCELLED"}

	# Get active shape key of selected object
	shape_key_origin = selected_object.active_shape_key_index

	# Transfer each shape key from selected object
	for i, key in enumerate(selected_object.data.shape_keys.key_blocks):
		selected_object.active_shape_key_index = i
		ops.object.shape_key_transfer()
	
	# Select original shape key for selected object
	selected_object.active_shape_key_index = shape_key_origin


def add_to_menu(self, context):
    # self.layout.separator()
    self.layout.operator(TransferAllShapeKeys.bl_idname, text="Transfer All Shape Keys", icon="COPY_ID")
	

class TransferAllShapeKeys(bpy.types.Operator):
	"""Transfer all shapes from one selected mesh object to the active mesh object"""
	bl_label = "Transfer All Shape Keys"
	bl_idname = "object.transfer_all_shape_keys"
	bl_options = {'REGISTER', 'UNDO'}

	@classmethod
	def poll(cls, context):
		return len(context.selected_objects) == 2

	def execute(self, context):
		main(self, context)	
		return {"FINISHED"}


def register():
	bpy.utils.register_module(__name__)
	bpy.types.MESH_MT_shape_key_specials.append(add_to_menu)

def unregister():
	bpy.utils.unregister_module(__name__)
	bpy.types.MESH_MT_shape_key_specials.remove(add_to_menu)

if __name__ == "__main__":
	register()

